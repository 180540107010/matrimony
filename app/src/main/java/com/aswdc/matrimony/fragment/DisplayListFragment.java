package com.aswdc.matrimony.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.activity.RegistrationActivity;
import com.aswdc.matrimony.adapter.DisplayListAdapter;
import com.aswdc.matrimony.adapter.GenderIconAdapter;
import com.aswdc.matrimony.database.User;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DisplayListFragment extends Fragment {

    ArrayList<UserModel> userList = new ArrayList<>();
    DisplayListAdapter adapter;
    @BindView(R.id.rcvDisplayList)
    RecyclerView rcvDisplayList;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;
    MyFavouriteChange myFavouriteChange = new MyFavouriteChange();

    public static DisplayListFragment getInstanceGender(int gender) {
        DisplayListFragment fragment = new DisplayListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_display_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myFavouriteChange,new IntentFilter(Constant.FAVOURITE_CHANGE_FILTER));
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myFavouriteChange);
    }

    public void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("ALERT!");
        alertDialog.setMessage("Delete this user permanently? ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                "Yes",
                (DialogInterface dialogInterface, int i) -> {
                    int deletedUserId = new User(getActivity()).deleteUserById(userList.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (DialogInterface dialogInterface, int i) -> {
            dialogInterface.dismiss();
        });
        alertDialog.show();
    }

    void setAdapter() {
        rcvDisplayList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userList.addAll(new User(getActivity()).getUserListByGender(getArguments().getInt(Constant.GENDER)));
        adapter = new DisplayListAdapter(getActivity(), userList, new DisplayListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavouriteClick(int position) {
                int updatedFavouriteStatus = new User(getActivity()).updateFavouriteStatus(userList.get(position).getIsFavourite() == 0 ? 1 : 0,
                        userList.get(position).getUserId());
                if(updatedFavouriteStatus > 0){
                    userList.get(position).setIsFavourite(userList.get(position).getIsFavourite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }
        });
        rcvDisplayList.setAdapter(adapter);
        checkAndVisibleView();
    }


    public void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvDisplayList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvDisplayList.setVisibility(View.GONE);
        }
    }

    void openAddUserScreen(){
        Intent intent = new Intent(getActivity(), RegistrationActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();
    }

    class  MyFavouriteChange extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra(Constant.USER_ID)){
                UserModel userModel = (UserModel) intent.getSerializableExtra(Constant.USER_ID);
                checkUserIdAndUpdateStatus(userModel.getUserId());
            }
        }
    }

    void checkUserIdAndUpdateStatus(int userId){
        for(int i =0;i<userList.size();i++){
            if(userId == userList.get(i).getUserId()){
                int isFavourite = userList.get(i).getIsFavourite();
                userList.get(i).setIsFavourite(isFavourite==0 ? 1:0);
                adapter.notifyItemChanged(i);
                return;
            }
        }
    }
}

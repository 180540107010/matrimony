package com.aswdc.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Utils;

import java.util.ArrayList;
import java.util.Calendar;

public class User extends MyDatabase {
    public final static String TABLE_NAME = "User";
    public final static  String USER_ID = "UserId";
    public final static  String NAME = "Name";
    public final static  String FATHER_NAME = "FatherName";
    public final static  String SURNAME = "Surname";
    public final static  String GENDER = "Gender";
    public final static  String DATE_OF_BIRTH = "DateOfBirth";
    public final static  String HOBBIES = "Hobbies";
    public final static  String LANGUAGE = "Language";
    public final static  String CITY_ID = "CityId";
    public final static  String PHONE_NUMBER = "PhoneNumber";
    public final static  String EMAIL = "Email";
    public final static  String IS_FAVOURITE = "IsFavourite";

    public final static String CITY = "CityName";
    public final static String AGE = "Age";
    public User(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<UserModel>();
        String query = " SELECT " +
                        " UserId," +
                        " Name," +
                        " FatherName," +
                        " Surname," +
                        " Gender," +
                        " DateOfBirth," +
                        " Language," +
                        " User.CityId," +
                        " PhoneNumber," +
                        " Email," +
                        " isFavourite," +
                        " Hobbies," +
                        " CityName," +
                        " isFavourite " +

                        " FROM " +
                        " User "  +
                        " INNER JOIN City ON User.CityId = City.CityId";
        Cursor cursor = db.rawQuery(query,null);
        if(cursor!= null){
            cursor.moveToFirst();
            for(int i=0;i<cursor.getCount();i++){
                list.add(getCreatedModelUsingCursor(cursor));
                cursor.moveToNext();
            }
        }

        cursor.close();
        db.close();
        return list;
    }

    public UserModel getCreatedModelUsingCursor(Cursor cursor){
        UserModel userModel = new UserModel();
        userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        userModel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        userModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        userModel.setSurname(cursor.getString(cursor.getColumnIndex(SURNAME)));
        userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        userModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
        userModel.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
        userModel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        userModel.setDateOfBirth(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DATE_OF_BIRTH))));
        userModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY)));
        userModel.setIsFavourite(cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE)));
        return  userModel;
    }

    public String setAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();
        return ageS;
    }

    public UserModel getUserById(int userId){
        SQLiteDatabase db = getReadableDatabase();
        UserModel userModel = new UserModel();
        String query =" SELECT * FROM " + TABLE_NAME +
                        " WHERE " +
                        " UserId = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(userId)});
        cursor.moveToFirst();
        userModel = getCreatedModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return userModel;
    }

    public ArrayList<UserModel> getUserListByGender(int gender){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
    //  String query = "SELECT * FROM "+ TABLE_NAME + " WHERE " + GENDER +" = ?";
        String query = " SELECT " +
                " UserId," +
                " Name," +
                " FatherName," +
                " Surname," +
                " Gender," +
                " DateOfBirth," +
                " Language," +
                " User.CityId," +
                " PhoneNumber," +
                " Email," +
                " isFavourite," +
                " Hobbies," +
                " CityName, " +
                " isFavourite " +

                " FROM " +
                " User "  +
                " INNER JOIN City ON User.CityId = City.CityId" +
                " WHERE " +
                " Gender = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for(int i =0; i < cursor.getCount() ;i++ ){
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getFavouriteUserList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
               " SELECT " +
                " UserId," +
                " Name ," +
                " FatherName," +
                " Surname," +
                " Gender," +
                " DateOfBirth," +
                " Language," +
                " User.CityId," +
                " PhoneNumber," +
                " Email," +
                " isFavourite," +
                " Hobbies," +
                " CityName," +
                " isFavourite " +

                " FROM " +
                " User "  +
                " INNER JOIN City ON User.CityId = City.CityId" +
                        " WHERE " +
                        " IsFavourite = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for(int i =0; i < cursor.getCount() ;i++ ){
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }


    public long insertUser(String name,String fatherName, String surname, int gender, String dateOfBirth,
                           String hobbies, String phoneNumber, String email,String  language,int cityId,int isFavourite){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME,name);
        cv.put(FATHER_NAME,fatherName);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(DATE_OF_BIRTH,dateOfBirth);
        cv.put(HOBBIES,hobbies);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(EMAIL,email);
        cv.put(LANGUAGE,language);
        cv.put(CITY_ID,cityId);
        cv.put(IS_FAVOURITE,isFavourite);
        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertedId;
    }

    public int updateUserByUserId(String name,String fatherName, String surname, int gender, String dateOfBirth,
                          String hobbies, String phoneNumber, String email, String language, int cityId,int isFavourite, int userId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME,name);
        cv.put(FATHER_NAME,fatherName);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(DATE_OF_BIRTH,dateOfBirth);
        cv.put(HOBBIES,hobbies);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(EMAIL,email);
        cv.put(LANGUAGE,language);
        cv.put(CITY_ID,cityId);
        cv.put(IS_FAVOURITE,isFavourite);
        int updatedUserId = db.update(TABLE_NAME,cv,USER_ID + " = ?",new String[]{String.valueOf(userId)});
        db.close();
        return updatedUserId;
    }

    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserID = db.delete(TABLE_NAME, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return deletedUserID;
    }

    public int updateFavouriteStatus(int isFavorite, int userId){
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(IS_FAVOURITE,isFavorite);
            int updatedFavouriteStatus = db.update(TABLE_NAME,cv,USER_ID + " =? ",new String[]{String.valueOf(userId)});
            db.close();
            return updatedFavouriteStatus;
    }

}

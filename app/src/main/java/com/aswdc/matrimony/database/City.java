package com.aswdc.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.matrimony.model.CityModel;

import java.util.ArrayList;
import java.util.HashMap;


public class City extends MyDatabase {
    public final static String TABLE_NAME = "City";
    public final static String CITY_ID = "CityId";
    public final static String CITY = "CityName";
    public City(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        CityModel cityModel1 = new CityModel();
        cityModel1.setCityName("Select One City");
        list.add(0, cityModel1);
        for (int i = 0; i < cursor.getCount(); i++) {
            CityModel cityModel = new CityModel();
           cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public CityModel getCityById(int cityId){
        SQLiteDatabase db = getReadableDatabase();
        CityModel model = new CityModel();
        String query = "SELECT * FROM " + TABLE_NAME +  " WHERE " + CITY_ID + " = ?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(cityId)});
        cursor.moveToFirst();
        model.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setCityName(cursor.getString(cursor.getColumnIndex(CITY)));
        cursor.close();
        db.close();
        return model;
    }
}

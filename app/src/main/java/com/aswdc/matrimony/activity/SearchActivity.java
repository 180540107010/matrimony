package com.aswdc.matrimony.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.DisplayListAdapter;
import com.aswdc.matrimony.adapter.GenderIconAdapter;
import com.aswdc.matrimony.database.User;
import com.aswdc.matrimony.fragment.DisplayListFragment;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseActivity{
    @BindView(R.id.etUserSearch)
    EditText etUserSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    Context context;
    GenderIconAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setupActionBar(getString(R.string.title_search_user),true);
        setAdapter();
        setSearchUser();
    }

    void resetAdapter(){
        if(adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }
    void setSearchUser(){
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempUserList.clear();
                if(charSequence.toString().length() > 0){
                    for(int j =0 ;j<userList.size();j++){
                        if(userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                 || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurname().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())){
                            tempUserList.add(userList.get(j));
                        }
                    }

                }
                if(tempUserList.size() == 0 && charSequence.toString().length() == 0){
                    tempUserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("ALERT!");
        alertDialog.setMessage("Delete this user permanently? ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                "Yes",
                (DialogInterface dialogInterface, int i) -> {
                    int deletedUserId = new User(SearchActivity.this).deleteUserById(userList.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(SearchActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();

                    } else {
                        Toast.makeText(SearchActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (DialogInterface dialogInterface, int i) -> {
            dialogInterface.dismiss();
        });
        alertDialog.show();
    }

    void setAdapter(){
        rcvUsers.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new User(this).getUserList());
        tempUserList.addAll(userList);
        adapter = new GenderIconAdapter(this, tempUserList, new GenderIconAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavouriteClick(int position) {
                int updateFavStatus = new User(SearchActivity.this).updateFavouriteStatus(userList.get(position).getIsFavourite() == 0 ? 1 : 0,userList.get(position).getUserId());
                if(updateFavStatus > 0){
                    userList.get(position).setIsFavourite(userList.get(position).getIsFavourite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }
        });
        rcvUsers.setAdapter(adapter);
        checkAndVisibleView();
    }
    public void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUsers.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUsers.setVisibility(View.GONE);
        }
    }
}
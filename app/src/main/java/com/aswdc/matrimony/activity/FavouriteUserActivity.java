package com.aswdc.matrimony.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.DisplayListAdapter;
import com.aswdc.matrimony.adapter.GenderIconAdapter;
import com.aswdc.matrimony.database.User;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteUserActivity extends BaseActivity {
    @BindView(R.id.rcvDisplayList)
    RecyclerView rcvDisplayList;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;
    ArrayList<UserModel> userList = new ArrayList<>();
    GenderIconAdapter adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        ButterKnife.bind(this);
        setupActionBar(getString(R.string.title_user_list), true);
        setAdapter();
    }

    public void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("ALERT!");
        alertDialog.setMessage("Delete this user permanently? ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                "Yes",
                (DialogInterface dialogInterface, int i) -> {
                    int deletedUserId = new User(FavouriteUserActivity.this).deleteUserById(userList.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(FavouriteUserActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();

                    } else {
                        Toast.makeText(FavouriteUserActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (DialogInterface dialogInterface, int i) -> {
            dialogInterface.dismiss();
        });
        alertDialog.show();
    }
    void setAdapter(){
        rcvDisplayList.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new User(this).getFavouriteUserList());
        adapter = new GenderIconAdapter(this, userList, new GenderIconAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavouriteClick(int position) {
                int updateFavStatus = new User(FavouriteUserActivity.this).updateFavouriteStatus(userList.get(position).getIsFavourite() == 0 ? 1 : 0,userList.get(position).getUserId());
                if(updateFavStatus > 0){
                    userList.get(position).setIsFavourite(userList.get(position).getIsFavourite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }
        });
        rcvDisplayList.setAdapter(adapter);
        checkAndVisibleView();
    }

    public void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvDisplayList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvDisplayList.setVisibility(View.GONE);
        }
    }

    void sendFavouriteChangeBroadcast(UserModel userModel){
        Intent intent = new Intent(Constant.FAVOURITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID, userModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}

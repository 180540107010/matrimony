package com.aswdc.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;

import com.aswdc.matrimony.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {


    @BindView(R.id.dashboard_cvActRegister)
    CardView dashboardCvActRegister;
    @BindView(R.id.dashboard_cvActDisplay)
    CardView dashboardCvActDisplay;
    @BindView(R.id.dashboard_cvActSearch)
    CardView dashboardCvActSearch;
    @BindView(R.id.dashboard_cvActFavourite)
    CardView dashboardCvActFavourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupActionBar(getString(R.string.dashboard_title),false);

    }

    @OnClick(R.id.dashboard_cvActRegister)
    public void onDashboardCvActRegisterClicked() {
        Intent intent = new Intent(this,RegistrationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_cvActDisplay)
    public void onDashboardCvActDisplayClicked() {
        Intent intent = new Intent(this,GenderWiseUserListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_cvActSearch)
    public void onDashboardCvActSearchClicked() {
        Intent intent = new Intent(this,SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.dashboard_cvActFavourite)
    public void onDashboardCvActFavouriteClicked() {
        Intent intent = new Intent(this, FavouriteUserActivity.class);
        startActivity(intent);
    }
}
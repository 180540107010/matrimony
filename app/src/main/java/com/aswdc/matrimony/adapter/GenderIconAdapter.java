package com.aswdc.matrimony.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.database.User;
import com.aswdc.matrimony.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderIconAdapter extends RecyclerView.Adapter<GenderIconAdapter.UserHolder> {
    Context context;
    ArrayList<UserModel> userList;
    OnViewClickListener onViewClickListener;

    public GenderIconAdapter(Context context, ArrayList<UserModel> userList,OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;
    }

    public interface OnViewClickListener{
        void onDeleteClick(int position);
        void onItemClick(int position);
        void onFavouriteClick(int position);
    }

    @NonNull
    @Override
    public GenderIconAdapter.UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.search_view_row,null));
    }

    @Override
    public void onBindViewHolder(@NonNull GenderIconAdapter.UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName()+ " " + userList.get(position).getFatherName() + " "+ userList.get(position).getSurname());
        holder.tvDob.setText(userList.get(position).getDateOfBirth()+ " | " + userList.get(position).getCityName());
        holder.ivFavouriteUser.setImageResource(userList.get(position).getIsFavourite() == 0 ? R.drawable.not_favourite_user : R.drawable.favourite_user);
        holder.ivGenderIcon.setImageResource(userList.get(position).getGender() == 1 ? R.drawable.male_icon : R.drawable.female_icon);
        holder.ivDeleteUser.setOnClickListener(view -> {
            if (onViewClickListener != null){
                onViewClickListener.onDeleteClick(position);
            }
        });
        holder.ivFavouriteUser.setOnClickListener(view -> {
            if (onViewClickListener != null){
                onViewClickListener.onFavouriteClick(position);
            }
        });
        holder.itemView.setOnClickListener(view -> {
            if (onViewClickListener != null){
                onViewClickListener.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvDob)
        TextView tvDob;
        @BindView(R.id.tvCity)
        TextView tvCity;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavouriteUser)
        ImageView ivFavouriteUser;
        @BindView(R.id.ivGenderIcon)
        ImageView ivGenderIcon;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}